solution "cppcon_example"
    configurations {
        "Debug",
        "Release",
    }

    platforms {
        "x64",
    }

    language "C++"

    PROJ_DIR = path.getabsolute("..")

    project "example1"
        kind "ConsoleApp"

        --debugdir (path.join(PROJ_DIR, "tests"))

        --includedirs {
        --    path.join(PROJ_DIR, ".."),
        --}

        files {
            path.join(PROJ_DIR, "*.cpp"),
        }

        configuration { "gmake"}
            buildoptions_cpp {
                "-x c++",
                "-std=c++14",
            }
